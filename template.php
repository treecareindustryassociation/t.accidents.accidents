<?php

/**
 * Implements hook_process_html().
 */
function accidents_preprocess_html(&$variables) {

  //dpm($variables);
  drupal_add_library('nice_menus', 'nice_menus');
  drupal_add_css(drupal_get_path('module', 'nice_menus') . '/css/nice_menus.css', array('group' => CSS_DEFAULT, 'basename' => 'nice_menus.css'));
  drupal_add_css(drupal_get_path('module', 'nice_menus') . '/css/nice_menus_default.css', array('group' => CSS_DEFAULT, 'basename' => '/css/nice_menus_default.css'));

}

/**
 * Implements hook_process_page().
 */
function accidents_preprocess_page(&$variables) {

//  dpm($variables);

  $variables['page']['grid']['outer'] = 24;
  $variables['page']['grid']['inner'] = 22;
  if ($variables['page']['sidebar_second']) {
    $variables['page']['grid']['outer'] = 19;
    $variables['page']['grid']['inner'] = 17;
  }

}