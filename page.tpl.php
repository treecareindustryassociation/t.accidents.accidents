<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

<div id="page" class="clearfix wrapper1">
  <div id="site-header" class="container-24 clearfix">
    <div class="clearfix">
      <div id="branding" class="grid-5">
        <a href="/">
          <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" title="<?php print $site_name; ?>" width="119" height="85" />
        </a>
      </div><!-- /#branding -->
      <?php //if ($page['header_secondary']): ?>
        <div id="top-navigation" class="grid-18 push-1">
          <div><?php //print render($page['header_secondary']); ?></div>
        </div>
      <?php //endif; ?>
      <?php //if ($page['header_first']): ?>
        <div id="header-first" class="grid-8 push-11">
          <div id="block-search-0" class="block block-search">
            <div class="content">
              <form action="/" accept-charset="UTF-8" method="post" id="search-block-form">
                <div>
                  <div class="container-inline">
                    <input name="" id="" class="form-submit" src="http://tcia.org/sites/tcia.org/themes/tcia/img/layout/search.png" type="image">
                    <div class="form-item" id="edit-search-block-form-1-wrapper">
                      <input maxlength="128" name="search_block_form" id="edit-search-block-form-1" size="29" value="Search Site" onblur="if (this.value == '') {this.value = 'Search Site';} this.style.color = '#666666';" onfocus="if (this.value == 'Search Site') {this.value = '';} this.style.color = '#666666';" class="form-text" type="text">
                    </div>
                    <input name="form_build_id" id="form-EghEGUzSX9txduIq6_CgQWqfYa-sPmVoRSUWXSsLO0E" value="form-EghEGUzSX9txduIq6_CgQWqfYa-sPmVoRSUWXSsLO0E" type="hidden">
                    <input name="form_token" id="edit-search-block-form-form-token" value="bfe38d4866d65f2839efa848fe6dc225" type="hidden">
                    <input name="form_id" id="edit-search-block-form" value="search_block_form" type="hidden">
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div id="block-block-7" class="block block-block">
            <div class="content">
              <p><a class="button" href="https://secure.tcia.org/join/">Join TCIA Now!</a></p>
            </div>
          </div>
          <?php //print render($page['header_first']); ?>
        </div><!-- /#header-first -->
      <?php //endif; ?>
      <?php //if ($page['header_last']): ?>
      <div class="clearfix">
        <div id="navigation" class="grid-24">
          <div id="block-nice_menus-1" class="block block-nice_menus">
            <div class="content"><!-- include navigation (http://tcia.org/3rdpartyaccess/primary-links) --></div>
          </div>
          <?php //print render($page['header_last']); ?>
        </div>
      </div>
      <?php //endif; ?>
    </div>
  </div><!-- /#header -->
  <div id="internal-nav" class="container-24 clearfix">
    <div id="bcrumb" class="grid-24">
      <div id="breadcrumb" class="breadcrumb"><a href="http://tcia.org">Home</a> &rsaquo; <a title="Safety" href="/safety">Safety</a> &rsaquo; <a title="Accidents" href="/accidents">Accidents</a> <?php print $breadcrumb; ?></div>
    </div>
    <div class="header-img grid-24"><img src="http://tcia.org/sites/tcia.org/themes/tcia/img/banners/rotating_3_resized.jpg" /></div>
  </div><!-- /#internal-nav -->
  <div id="main-content-container" class="container-24 clearfix">
    <div id="main-wrapper-outer" class="clearfix">
      <?php if($messages): ?>
        <div id="msgs" class="container-24 clearfix">
          <div class="grid-24 alpha omega">
            <?php print $messages; ?>
          </div>
        </div><!-- /#msgs -->
      <?php endif; ?>
      <div id="main-wrapper" class="column grid-<?php print $page['grid']['outer']; ?> alpha omega">
        <div id="main-wrapper-inner" class="column grid-<?php print $page['grid']['inner']; ?> push-1 alpha omega">
          <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
          <a id="main-content"></a>
          <?php print render($title_prefix); ?>
          <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
          <?php print render($title_suffix); ?>
          <div id="main-content" class="region clearfix">
            <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
            <?php print render($page['help']); ?>
            <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
            <?php print render($page['content']); ?>
            <?php print $feed_icons; ?>
          </div><!-- /#main-content -->
        </div>
      </div><!-- /#main-wrapper -->
      <?php if ($page['sidebar_second']): ?>
        <div id="sidebar-last" class="column sidebar region grid-5 tcia-sidebar-last alpha omega">
          <?php print render($page['sidebar_second']); ?>
        </div><!-- /#sidebar-last -->
      <?php endif; ?>
    </div>
  </div><!-- /#main-content-container -->
</div><!-- /#page -->
<div id="footer-full">
  <div id="footer-inner">
    <div id="footer-wrapper" class="container-24 clearfix">
      <div id="footer-first" class="grid-7">
        <div id="block-block-2" class="block block-block">
          <div class="content">
            <div>
              <p><strong>Partners Advancing Commercial Tree Care</strong></p>
              <p>TCIA would like to thank the following crown partners whose commitment to our work is extraordinary.</p>
            </div>
            <p><a href="http://tcia.org/about-us/pact-partners"><img alt="" src="http://tcia.org/sites/tcia.org/themes/tcia/img/layout/pact.png" width="270" height="65" /></a></p>
          </div>
        </div>
      </div><!-- /#footer-first -->
      <div id="footer-last" class="grid-10 clearfix">
        <div id="block-menu_block-4" class="block block-menu_block">
          <h2>Quick Links</h2>
          <div class="content">
            <?php print render($page['footer']); ?>
          </div>
        </div>
      </div><!-- /#footer-last -->
      <div id="footer-right-outer" class="grid-7">
        <div id="footer-message" class="grid-7">
          <div id="footer-logo">
            <a href="http://www.tcia.org" target="_blank">Tree Care Industry Association</a>
          </div>
          <p>&copy; 2012 Tree Care Industry Association, Inc.<br />136 Harvey Road, Suite 101<br /> Londonderry, NH 03053<br />1-800-733-2622<br><a href="http://tcia.org/about-us/privacy-policy">Privacy Policy</a></p>
        </div><!-- /#footer-message -->
      </div>
    </div><!-- /#footer-wrapper -->
  </div>
</div><!-- /#footer-full -->
<script type="text/javascript">
  // add in menu settings
  jQuery.extend(Drupal.settings, {"nice_menus_options":{"delay":"400","speed":"fast"}});

  (function($) {
    //switch for http(s)
    if (document.location.protocol == "file:"){ var protocol = "http:"; }
    else { var protocol = document.location.protocol; }

    var svc = "//secure.tcia.org/FetchPage.aspx?page=";
    var cback = "&callback=?";

    // load primary navigation
    $.getJSON(protocol + svc + "TCIAprimaryJSON" + cback, function(data){
      $('#navigation #block-nice_menus-1 .content').html(data.result);
      $('ul.nice-menu').superfish({
        hoverClass: 'over',
        autoArrows: false,
        dropShadows: false,
        delay: Drupal.settings.nice_menus_options.delay,
        speed: Drupal.settings.nice_menus_options.speed
      }).find('ul').bgIframe({opacity:false});
      $('ul.nice-menu ul').css('display', 'none');
    });

    // load secondary navigation
    $.getJSON(protocol + svc + "TCIAsecondaryJSON" + cback, function(data){
      $('#top-navigation div').html(data.result);
    });

    // load footer navigation
    $.getJSON(protocol + svc + "TCIAfooterJSON" + cback, function(data){
      $('#footer-last #block-menu_block-4 .content').html(data.result);
    });
  }(jQuery));
</script>
